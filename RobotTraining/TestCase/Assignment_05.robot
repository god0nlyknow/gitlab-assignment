*** Settings ***
Library     RequestsLibrary
Library     JSONLibrary


*** Test Case ***
TC_Get_All_Asset
    Create session      alias=get_asset     url=http://125.26.15.143:8082
    ${response}=        Get on session      get_asset           /assets         expected_status=200
    Log to console      ${response}
    Should Be Equal As Strings              ${response.status_code}             200

    Log to console      ${response.json()}
    ${get_assetId}=     Get Value From JSON         ${response.json()}          $[?(@.assetId==a1)]
    IF  ${get_assetId} == [${EMPTY}]
        Log to console      Not found asset assetId
    ELSE
        Log to console      ${get_assetId}
    END