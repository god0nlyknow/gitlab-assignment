*** Settings ***
Resource        ${CURDIR}/../Keywords/commons.robot
Resource        ${CURDIR}/../Keywords/Pages/mainPage_Page.robot
Resource        ${CURDIR}/../Keywords/Features/mainPage_Feature.robot
Resource        ${CURDIR}/../Keywords/Pages/searchResultPage_Page.robot


*** Variables ****
${webURL}       https://www.makroclick.com/th
${inputTextInSearchBox}     เนื้อหมู
${nameOfProduct}        ซีพี หมูบดแช่แข็ง 1000 กรัม


*** Test Cases ***
Assignment 03
    Open Web Browser  ${webURL} 
    Close Makro Popup In Main Page
    # Input Text In Makro Search Box  ${inputTextInSearchBox}
    # Click Search In Makro Main Page
    Click Search After Input Text In Makro Main Page  ${inputTextInSearchBox}
    Add One Item With + Button  ${nameOfProduct}