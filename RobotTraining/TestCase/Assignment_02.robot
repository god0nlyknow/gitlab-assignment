*** Settings ***
Library     SeleniumLibrary

*** Test Cases ***
1. Assignment 02 
    Open Browser        url=https://www.ebay.com/     browser=gc
    Wait Until Element Is Visible       xpath=//div[contains(@id,'gh-ac-box2')]//input[contains(@class,'autocomplete-input')]
    Input Text          xpath=//div[contains(@id,'gh-ac-box2')]//input[contains(@class,'autocomplete-input')]        Rem
    Wait Until Element Is Visible       xpath=//form[contains(@id,'gh-f')]//input[contains(@type,'submit')]
    Click Element       xpath=//form[contains(@id,'gh-f')]//input[contains(@type,'submit')]

    ${index}=    Set Variable    1
    @{elements}     Get WebElements     //div[contains(@class,'item__wrapper')]//h3[contains(@class,'s-item__title')]
    FOR    ${element}     IN    @{elements}
        ${textFromScreen}       Get Text        ${element}
        Log To Console  ${textFromScreen}
        Exit For Loop If    ${index} >= 2
        ${index}=    Evaluate    ${index} + 1
    END