*** Settings ***
Resource        ${CURDIR}/../Keywords/commons.robot
Resource        ${CURDIR}/../Keywords/Pages/mainPage_Page.robot
Resource        ${CURDIR}/../Keywords/Features/mainPage_Feature.robot
Resource        ${CURDIR}/../Keywords/Pages/searchResultPage_Page.robot

Suite Teardown  Close Browser


*** Variables ****
${select_enviroment}    production
@{Products}      ตู้เย็น       ทีวี      เครื่องดูดฝุ่น
${productThatIWant}     ทีวี


*** Test Cases ***
Assignment 04
    #Open Web Site With IF Function Input Text  ${select_enviroment}    #I don't quite understand Assignment so I create 2 keywords
    Open Web Site With IF Function yaml file
    Close Makro Popup In Main Page
    # Input Text In Search Box With For Loop      ${productThatIWant}     @{Products}
    # Click Search In Makro Main Page
    Input Text In Search Box With For Loop And Click Search  ${productThatIWant}  @{Products}
    List of Product Name And Product Code