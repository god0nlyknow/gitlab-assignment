*** Settings ***
Library     SeleniumLibrary

*** Test Cases ***
1. First Test 
    Open Browser        url=https://www.google.com/     browser=gc
    Wait Until Element Is Visible       xpath=//form[contains(@action,'search')]//input[contains(@title,'ค้นหา')]
    Input Text          xpath=//form[contains(@action,'search')]//input[contains(@title,'ค้นหา')]        ebay
    Wait Until Element Is Visible       xpath=//div[contains(@class,'FPdoLc')]//input[contains(@aria-label,'ค้นหาด้วย Google')]
    Click Element       xpath=//div[contains(@class,'FPdoLc')]//input[contains(@aria-label,'ค้นหาด้วย Google')]
    Go To               url=https://www.ebay.com/
    Wait Until Page Contains            Top Catagories
    #Click Element       xpath=//div[contains(@id,'destinations')]//h2
    #Close Browser