*** Settings ***
Library     SeleniumLibrary
Variables       ${CURDIR}/../Config/Config.yaml


*** Keywords ***
Open Web Browser
    [Arguments]         ${webURL}
    Open Browser        url=${webURL}         browser=gc
    Maximize Browser Window

Open Web Site With IF Function Input Text
    [Arguments]         ${select_enviroment}
    IF          '${select_enviroment}' == 'qa'
        Open Browser        url=${config['URL']['qa']}            browser=gc
        Maximize Browser Window
    ELSE IF     '${select_enviroment}' == 'dev'
        Open Browser        url=${config['URL']['dev']}           browser=gc
        Maximize Browser Window
    ELSE IF     '${select_enviroment}' == 'production'
        Open Browser        url=${config['URL']['production']}    browser=gc
        Maximize Browser Window
    ELSE
        Log To Console      Not Match
    END

Open Web Site With IF Function yaml file
    IF          '${config['select_enviroment']}' == 'qa'
        Open Browser        url=${config['URL']['qa']}            browser=gc
        Maximize Browser Window
    ELSE IF     '${config['select_enviroment']}' == 'dev'
        Open Browser        url=${config['URL']['dev']}           browser=gc
        Maximize Browser Window
    ELSE IF     '${config['select_enviroment']}' == 'production'
        Open Browser        url=${config['URL']['production']}    browser=gc
        Maximize Browser Window
    ELSE
        Log To Console      Not Match
    END