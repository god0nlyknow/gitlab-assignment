*** Settings ***
Library     SeleniumLibrary


*** Keywords ***
Close Makro Popup In Main Page
    Wait Until Element Is Visible       xpath=//a[contains(@data-dismiss,'modal')]//ancestor::div[contains(@id,'eventPopupModal')]//img[contains(@alt,'CloseModal')]
    Click Element       xpath=//a[contains(@data-dismiss,'modal')]//ancestor::div[contains(@id,'eventPopupModal')]//img[contains(@alt,'CloseModal')]

Input Text In Makro Search Box
    [Arguments]         ${inputTextInSearchBox}
    Wait Until Element Is Visible   xpath=//div[contains(@class,'search')]//input[contains(@class,'Input')]
    Input Text          xpath=//div[contains(@class,'search')]//input[contains(@class,'Input')]       ${inputTextInSearchBox}

Click Search In Makro Main Page
    Wait Until Element Is Visible       xpath=//div[contains(@class,'search')]//i[.= 'search']
    Click Element       xpath=//div[contains(@class,'search')]//i[.= 'search']

Input Text In Search Box With For Loop
    [Arguments]         ${productThatIWant}     @{Products}         
    FOR     ${Product}  IN  @{Products}
        IF    '${Product}' == '${productThatIWant}'
            Wait Until Element Is Visible   xpath=//div[contains(@class,'search')]//input
            Input Text          xpath=//div[contains(@class,'search')]//input        ${Product}
        END
    END