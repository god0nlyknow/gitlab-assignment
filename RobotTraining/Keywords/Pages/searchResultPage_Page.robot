*** Settings ***
Library         SeleniumLibrary


*** Keywords ***
Add One Item With + Button
    [Arguments]         ${nameOfProduct}
    Wait Until Element Is Visible   xpath=//div[.='${nameOfProduct}']//ancestor::div[contains(@class,'ProductCard')]//button[contains(@id,'plus')]
    Click Element       xpath=//div[.='${nameOfProduct}']//ancestor::div[contains(@class,'ProductCard')]//button[contains(@id,'plus')]

List of Product Name And Product Code
    @{product_elements}    Get WebElements     //div[contains(@class, "ProductCard__TitleMaxLines")]
    @{product_code_elements}        Get WebElements         //div[contains(@class, "ProductCard__TextCode")]
    ${index}=    Set Variable    0
    ${count_loop}=    Set Variable    0

    FOR    ${element}     IN    @{product_elements}
        ${product_name}       Get Text        ${element}
        Log To Console  ${product_name}
        ${product_code}       Get Text        ${product_code_elements}[${index}]
        Log To Console  ${product_code}
        ${index}=    Evaluate    ${index} + 1
        Exit For Loop If    ${count_loop} >= 4
        ${count_loop}=    Evaluate    ${count_loop} + 1
    END