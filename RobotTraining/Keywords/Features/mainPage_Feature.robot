*** Settings ***
Resource        ${CURDIR}/../Pages/mainPage_Page.robot

*** Keywords ***
Click Search After Input Text In Makro Main Page
    [Arguments]         ${inputTextInSearchBox}
    Input Text In Makro Search Box  ${inputTextInSearchBox}
    Click Search In Makro Main Page

Input Text In Search Box With For Loop And Click Search
    [Arguments]         ${productThatIWant}     @{Products}   
    Input Text In Search Box With For Loop  ${productThatIWant}  @{Products}
    Click Search In Makro Main Page